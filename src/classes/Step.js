import Object from './Object';

export default class Step extends Object
{
  constructor(id = 0, name, order)
  {
    super(id, order)
    this._done = false;
    this._name = name;
  }

  static formatFormServer(serverStep)
  {
    let step     = new Step(serverStep.id, serverStep.name, serverStep.order)
    step.done   = JSON.parse(serverStep.done)

    return step
  }

  toJSON() {
    return JSON.stringify({
      formatter: 'step',
      id:   this.id,
      done: this.done,
      name: this.name,
      order: this.order,
    })
  }

  get done()
  {
    return this._done;
  }

  set done(value)
  {
    this._done = value;
  }
  get name()
  {
    return this._name;
  }

  set name(value)
  {
    this._name = value;
  }


  get order()
  {
    return this._order;
  }

  set order(value)
  {
    this._order = value;
  }
}
