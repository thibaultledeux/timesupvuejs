import Object from './Object'
import Step from './Step'
import Word from './Word'
import Team from './Team'

export default class Room extends Object
{
  constructor(id = 0, name)
  {
    super(id)
    this._limitNbWords     = 5
    this._teamPlayingOrder = 0
    this._chrono           = 30
    this._teams            = []
    this._words            = []
    this._name             = name
    this._steps            = [
      new Step(0, 'Tout sauf les mots racines', 0),
      new Step(0, 'Un seul mot', 1),
      new Step(0, 'Mimes', 2),
    ]
  }

  static formatFormServer (serverRoom)
  {
    let room              = new Room(serverRoom.id)
    room.name             = serverRoom.name
    room.chrono           = parseInt(serverRoom.chrono)
    room.limitNbWords     = parseInt(serverRoom.limitNbWords)
    room.teamPlayingOrder = parseInt(serverRoom.teamPlayingOrder)

    serverRoom.teams.forEach((serverTeam) => {
      serverTeam = JSON.parse(serverTeam)
      room.addTeam(Team.formatFormServer(serverTeam))
    })
    room.teams.sort(Object.sortByOrder)

    serverRoom.words.forEach((serverWord) => {
      serverWord = JSON.parse(serverWord)
      room.addWord(Word.formatFormServer(serverWord))
    })

    room.steps = []
    serverRoom.steps.forEach((serverStep) => {
      serverStep = JSON.parse(serverStep)
      room.addStep(Step.formatFormServer(serverStep))
    })

    room.steps.sort(Object.sortByOrder)

    return room
  }

  /**
   *
   * @param {Team}  team
   */
  addTeam (team)
  {
    this._teams.push(team)
  }

  /**
   *
   * @param {Word}  word
   */
  addWord (word)
  {
    this._words.push(word)
  }

  /**
   *
   * @param {Step}  step
   */
  addStep (step)
  {
    this._steps.push(step)
  }

  toJSON()
  {
    let roomJson = {
      formatter: 'room',
      id: this.id,
      chrono: this.chrono,
      name: this.name,
      teamPlayingOrder: this.teamPlayingOrder,
      limitNbWords: this.limitNbWords,
      teams: [],
      words: [],
      steps: [],
    }

    this.teams.forEach((team) => {
      roomJson.teams.push(team.toJSON())
    })
    this.words.forEach((word) => {
      roomJson.words.push(word.toJSON())
    })
    this.steps.forEach((step) => {
      roomJson.steps.push(step.toJSON())
    })

    return JSON.stringify(roomJson)
  }

  /**
   * ajoute un point a la team du joueur
   *
   * @param  {Team}  teamAddPoint  la team a laquelle on ajoute le point
   */
  addPointToTeam (teamAddPoint) {
    this.teams.forEach((team) => {
      if (team.id === teamAddPoint.id)
      {
        team.score++
      }
    })
  }

  /**
   * set un mot done
   *
   * @param  {Word}  wordDone  mot fait
   */
  setWordDone (wordDone) {
    this.words.forEach((word) => {
      if (word.id === wordDone.id)
      {
        word.done = true
      }
    })
  }

  /**
   * restart les mots
   */
  restartWords () {
    this.words.forEach((word) => {
        word.done = false
    })
  }

  /**
   * recupère letape suivante
   *
   * @returns {Step|null}
   */
  nextStep()
  {
    let nextStep = null
    this._steps.forEach((step) => {
      if (nextStep === null && step.done === false)
      {
        nextStep = step
      }
    })

    return nextStep
  }

  get chrono()
  {
    return this._chrono
  }

  set chrono(value)
  {
    this._chrono = value
  }

  get teams()
  {
    return this._teams
  }

  get words()
  {
    return this._words
  }

  set words(value)
  {
    this._words = value
  }


  get limitNbWords()
  {
    return this._limitNbWords
  }

  set limitNbWords(value)
  {
    this._limitNbWords = value
  }

  get steps()
  {
    return this._steps
  }

  set steps(value)
  {
    this._steps = value
  }

  get teamPlayingOrder()
  {
    return this._teamPlayingOrder
  }

  set teamPlayingOrder(value)
  {
    this._teamPlayingOrder = value
  }

  get name()
  {
    return this._name;
  }

  set name(value)
  {
    this._name = value;
  }
}
