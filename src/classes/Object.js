export default class Object
{
  constructor(id = 0, order = null)
  {
    this.id     = id
    this._order = (order !== null) ? parseInt(order) : null
  }

  toString () {
    return JSON.stringify(this)
  }

  static sortByOrder(objectA, objectB)
  {
    if (objectA.order < objectB.order)
    {
      return -1
    }
    else if (objectA.order > objectB.order)
    {
      return 1
    }

    return 0
  }

  get id()
  {
    return this._id
  }

  set id(value)
  {
    if (value === 0)
    {
      const now = new Date()
      this._id = now.getTime() + Math.floor(Math.random() * Math.floor(100))
    }
    else
    {
      this._id = parseInt(value)
    }
  }


  get order()
  {
    return this._order
  }

  set order(value)
  {
    this._order = value
  }
}
