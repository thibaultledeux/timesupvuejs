import Object from './Object';

export default class Word extends Object
{
  constructor(id = 0, text)
  {
    super(id)
    this._done = false;
    this._text = text;
  }


  static formatFormServer(serverWord)
  {
    let word = new Word(serverWord.id, serverWord.text)
    word.done = JSON.parse(serverWord.done)

    return word
  }

  toJSON () {
    return JSON.stringify({
      formatter: 'word',
      id:   this.id,
      done: this.done,
      text: this.text,
    })
  }

  get text()
  {
    return this._text;
  }

  set text(value)
  {
    this._text = value;
  }

  get done()
  {
    return this._done;
  }

  set done(value)
  {
    this._done = value;
  }
}
