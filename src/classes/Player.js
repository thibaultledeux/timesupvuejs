import Object from './Object'

export default class Player extends Object
{
  constructor(id = 0, name, order)
  {
    super(id, order)
    this._name        = name
    this._ready       = false
    this._playing     = false
    this._isLastReady = false
  }


  toJSON () {
    return JSON.stringify({
      formatter:   'player',
      id:          this.id,
      name:        this.name,
      ready:       this.ready,
      order:       this.order,
      playing:     this.playing,
      isLastReady: this.isLastReady,
    })
  }

  static formatFormServer(serverPlayer)
  {
    let player         = new Player(serverPlayer.id, serverPlayer.name, serverPlayer.order)
    player.ready       = JSON.parse(serverPlayer.ready)
    player.playing     = JSON.parse(serverPlayer.playing)
    player.isLastReady = JSON.parse(serverPlayer.isLastReady)

    return player
  }


  get name()
  {
    return this._name
  }

  set name(value)
  {
    this._name = value
  }

  get ready()
  {
    return this._ready
  }

  set ready(value)
  {
    this._ready = value
  }

  get playing()
  {
    return this._playing
  }

  set playing(value)
  {
    this._playing = value
  }


  get order()
  {
    return this._order
  }

  set order(value)
  {
    this._order = value
  }


  get isLastReady()
  {
    return this._isLastReady
  }

  set isLastReady(value)
  {
    this._isLastReady = value
  }
}
