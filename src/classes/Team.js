import Object from './Object'
import Player from './Player';

export default class Team extends Object
{
  constructor(id = 0, order)
  {
    super(id, order)
    this._score              = 0;
    this._playerPlayingOrder = 0;
    this._players            = [];
  }

  static formatFormServer(serverTeam)
  {
    let team                = new Team(serverTeam.id, serverTeam.order);
    team.score              = parseInt(serverTeam.score);
    team.playerPlayingOrder = parseInt(serverTeam.playerPlayingOrder);

    serverTeam.players.forEach((serverPlayer) => {
      serverPlayer = JSON.parse(serverPlayer)
      team.addPlayer(Player.formatFormServer(serverPlayer))
    })
    team.players.sort(Object.sortByOrder)

    return team
  }


  toJSON () {
    let teamJSON = {
      formatter: 'team',
      id: this.id,
      score: this.score,
      playerPlayingOrder: this.playerPlayingOrder,
      order: this.order,
      players: [],
    }

    this.players.forEach((player) => {
      teamJSON.players.push(player.toJSON())
    })

    return JSON.stringify(teamJSON)
  }

  /**
   * ajoute un joueur a la team
   *
   * @param {Player}  player  un joueur
   */
  addPlayer(player)
  {
    this._players.push(player)
  }

  get players()
  {
    return this._players
  }


  set players(value)
  {
    this._players = value
  }


  get score()
  {
    return this._score
  }

  set score(value)
  {
    this._score = value
  }


  get order()
  {
    return this._order;
  }

  set order(value)
  {
    this._order = value;
  }


  get playerPlayingOrder()
  {
    return this._playerPlayingOrder;
  }

  set playerPlayingOrder(value)
  {
    this._playerPlayingOrder = value;
  }
}
