export default {
  setRooms: (state, rooms) => {
    state.rooms = rooms
  },
  setPlayerCurrent: (state, playerCurrent) => {
    state.playerCurrent = playerCurrent
    localStorage.playerCurrentId = playerCurrent.id
  },
  setRoomSelected: (state, roomSelected) => {
    state.roomSelected = roomSelected
    localStorage.roomSelectedId = roomSelected.id
    state.stepCurrent  = roomSelected.nextStep()
    state.roomSelected.teams.forEach((team) => {
      if (
        (state.teamSelected !== null && team.id === state.teamSelected.id) ||
        (typeof localStorage.teamSelectedId !== 'undefined' && team.id === Number(localStorage.teamSelectedId))
      )
      {
        state.teamSelected = team
        localStorage.teamSelectedId = team.id
        team.players.forEach((player) => {
          if ((state.playerCurrent !== null && player.id === state.playerCurrent.id) ||
            (typeof localStorage.playerCurrentId !== 'undefined' && player.id === Number(localStorage.playerCurrentId))
          )
          {
            state.playerCurrent = player
            localStorage.playerCurrentId = player.id
          }
        })
      }
    })
  },
  setTeamSelected: (state, teamSelected) => {
    state.teamSelected = teamSelected
    localStorage.teamSelectedId = teamSelected.id
  },
  addPointToTeam: (state, team) => {
    state.roomSelected.addPointToTeam(team)
  },
  setWordDone: (state, word) => {
    state.roomSelected.setWordDone(word)
  },
  restartWords: (state) => {
    state.roomSelected.restartWords()
  },
  setNullRoomSelected: (state) => {
    state.roomSelected = null
    localStorage.removeItem('roomSelectedId')
  },
  setNullTeamSelected: (state) => {
    state.teamSelected = null
    localStorage.removeItem('teamSelectedId')
  },
  setNullPlayerCurrent: (state) => {
    state.playerCurrent = null
    localStorage.removeItem('teamSelectedId')
  },
  setTrueWaiting: (state) => {
    state.waiting = true
  },
  setFalseWaiting: (state) => {
    state.waiting = false
  },
  setExistWords: (state, existWords) => {
    state.existWords     = existWords
    state.wordsValidated = (state.existWords.length === 0)
  },
  addPendingRequest: (state, pendingRequest) => {
    state.pendingRequests.push(pendingRequest)
  },
  cleanPendingRequests: (state, pendingRequest) => {
    state.pendingRequests = []
  },
  setFalseWordsValidated: (state) => {
    state.wordsValidated = false
  },
  cleanGame: (state) => {
    state.roomSelected = null
    state.teamSelected = null
    state.playerCurrent = null
    localStorage.clear()
  },


}
