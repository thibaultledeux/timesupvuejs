import SocketResponse from '../utils/SocketResponse';
import Router from './../router/index'

export default {
  sendRequest (store, socketRequest) {
    if (this._vm.$socket.client.connected)
    {
      console.log('sendRequest')
      console.log(socketRequest)
      this._vm.$socket.client.emit('request', socketRequest)
    }
    else
    {
      console.log('socket non connecté')
      console.log('-> request add in pending request')
      store.commit('addPendingRequest', socketRequest)
    }
  },
  socket_connect ({ commit, getters }, response) {
    console.log('socket connected')
    if (getters.pendingRequests.length > 0) {
      getters.pendingRequests.forEach((pendingRequest) => {
        console.log('pendingRequest')
        console.log(pendingRequest)
        this._vm.$socket.client.emit('request', pendingRequest)
      })
      commit('cleanPendingRequests')
    }
  },
  socket_disconnect ({ store, getters }, response) {
    console.log('socket disconnect')
    if (getters.roomSelected) {
      localStorage.roomSelectedId = getters.roomSelected.id
    }
    if (getters.teamSelected) {
      localStorage.teamSelectedId = getters.teamSelected.id
    }
    if (getters.playerCurrent) {
      localStorage.playerCurrentId = getters.playerCurrent.id
    }
  },
  socket_response (store, response) {
    try
    {
      const socketResponse = new SocketResponse(
        response._roomId,
        response._action,
        response._data,
        response._success,
        response._redirect,
        response._routeOrigin
      )
      console.log('response', socketResponse)
      if (socketResponse.success === true )
      {
        if (socketResponse.action !== null)
        {
          let data = socketResponse.formatData()
          store.commit(socketResponse.action, data)
        }

        if (socketResponse.redirect !== null)
        {
          Router.push({name: socketResponse.redirect})
        }
      }
      else if (socketResponse.success === false)
      {
        throw new Error(`socket request error route origin ${socketResponse.origin}`)
      }

      // setTimeout(() => {
      //    store.commit('setFalseWaiting')
      //  }, 500)

    }
    catch (e)
    {
      console.log(e)
      // setTimeout(() => {
      //   store.commit('setFalseWaiting')
      // }, 500)
    }
  },
  cleanGame (store) {
    store.commit('cleanGame')
  },
  setPlayerCurrent (store, player) {
    store.commit('setPlayerCurrent', player)
  },
  setRoomSelected (store, room) {
    store.commit('setRoomSelected', room)
  },
  setTeamSelected (store, team) {
    store.commit('setTeamSelected', team)
  },
  addPointToTeam (store, team) {
    store.commit('addPointToTeam', team)
  },
  setWordDone (store, word) {
    store.commit('setWordDone', word)
  },
  restartWords (store) {
    store.commit('restartWords')
  },
  setNullRoomSelected (store) {
    store.commit('setNullRoomSelected')
  },
  setNullTeamSelected (store) {
    store.commit('setNullTeamSelected')
  },
  setNullPlayerCurrent (store) {
    store.commit('setNullPlayerCurrent')
  },
  setTrueWaiting (store) {
    store.commit('setTrueWaiting')
  },
  setFalseWaiting (store) {
    store.commit('setFalseWaiting')
  },
  setExistWords (store, existWords) {
    store.commit('setExistWords', existWords)
  },
  setFalseWordsValidated (store) {
    store.commit('setFalseWordsValidated')
  },

}
