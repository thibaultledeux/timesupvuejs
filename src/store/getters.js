export default {
  stepCurrent:    (state) => state.stepCurrent,
  teamSelected:   (state) => state.teamSelected,
  roomSelected:   (state) => state.roomSelected,
  rooms:          (state) => state.rooms,
  playerCurrent:  (state) => state.playerCurrent,
  waiting:        (state) => state.waiting,
  existWords:     (state) => state.existWords,
  wordsValidated: (state) => state.wordsValidated,
  pendingRequests: (state) => state.pendingRequests,
}
