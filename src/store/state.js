export default {
  pendingRequests: [],
  rooms:           [],
  existWords:      [],
  stepCurrent:     null,
  roomSelected:    null,
  teamSelected:    null,
  playerCurrent:   null,
  waiting:         false,
  wordsValidated:  false,
}
