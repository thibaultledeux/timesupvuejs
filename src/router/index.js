import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import CreateRoom from '@/components/CreateRoom'
import JoinRoom from '@/components/JoinRoom'
import TeamsList from '@/components/TeamsList'
import PlayersList from '../components/PlayersList';
import Game from '../components/Game';
import CreateWords from '../components/CreateWords';
import Rules from '../components/Rules';

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/createRoom',
      name: 'createRoom',
      component: CreateRoom
    },
    {
      path: '/joinRoom',
      name: 'joinRoom',
      component: JoinRoom
    },
    {
      path: '/teamsList',
      name: 'teamsList',
      component: TeamsList
    },
    {
      path: '/playersList',
      name: 'playersList',
      component: PlayersList
    },
    {
      path: '/createWords',
      name: 'createWords',
      component: CreateWords
    },
    {
      path: '/game',
      name: 'game',
      component: Game
    },
    {
      path: '/rules',
      name: 'rules',
      component: Rules
    },
    {
      path: '*',
      redirect: '/',
    },
  ]
})
