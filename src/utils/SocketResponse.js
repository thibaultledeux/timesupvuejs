import Room from '../classes/Room'
import Word from '../classes/Word'

export default class SocketResponse
{
  constructor(roomId = 0, action, data, success, redirect, routeOrigin)
  {
    this._data        = data
    this._action      = action
    this._roomId      = roomId
    this._success     = success
    this._redirect    = redirect
    this._routeOrigin = routeOrigin
  }

  formatData()
  {
    let data = null
    switch (this._action)
    {
      case `setRooms`:
        data = []
        this._data.forEach((serverRoom) => {
          serverRoom = JSON.parse(serverRoom)
          data.push(Room.formatFormServer(serverRoom))
        })
        break

      case `setRoomSelected`:
        let roomSelected = JSON.parse(this._data)
        data = Room.formatFormServer(roomSelected)
        break

      case `setExistWords`:
        data = []
        this._data.forEach((item) => {
          item = JSON.parse(item)
          data.push(Word.formatFormServer(item))
        })
        break

      default:
        throw new Error(`action inconnue ${this._action}`)
    }
    console.log('data response')
    console.log(data)

    return data
  }


  get action()
  {
    return this._action
  }

  get success()
  {
    return this._success
  }

  get redirect()
  {
    return this._redirect
  }


  get routeOrigin()
  {
    return this._routeOrigin;
  }
}
