export default class SocketRequest
{
  constructor(roomId = 0, route, data = null)
  {
    this._roomId = roomId;
    this._route  = route;
    this._data   = data;
  }

  toJSON () {
    return JSON.stringify({
      roomId: this.roomId,
      route: this.route,
      data: this.data,
    })
  }

  get roomId()
  {
    return this._roomId;
  }

  get route()
  {
    return this._route;
  }

  get data()
  {
    return this._data;
  }
}
