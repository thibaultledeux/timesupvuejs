// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import router from './router'
import VueSocketIOExt from 'vue-socket.io-extended'
import timesupStore from './store/timesupStore'
import socketClient from './socket/socketClient'

Vue.config.productionTip = false

Vue.use(VueSocketIOExt, socketClient, { store: timesupStore })


/* eslint-disable no-new */
new Vue({
  store: timesupStore,
  el: '#app',
  router,
  components: { App },
  template: '<App />',
  methods: {
    preventNav(event) {
      event.preventDefault()
      event.returnValue = ''
    }
  },
  beforeMount() {
    // window.addEventListener(`beforeunload`, this.preventNav)
  },

  beforeDestroy() {
    // window.removeEventListener(`beforeunload`, this.preventNav)
    // this.$socket.removeListener('response')
  },


})
